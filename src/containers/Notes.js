import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { getFilteredNotes } from '../reducers/notes'
import { deleteNote, editNote, saveNote, fetchNotes, createNote } from '../actions/actionCreators'
import Notes from '../components/Notes'

const mapStateToProps = state => ({
  list: getFilteredNotes(state),
})

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      onDeletePress: deleteNote,
      onEditPress: editNote,
      onSavePress: saveNote,
      onInit: fetchNotes,
      onAddPress: createNote,
    },
    dispatch
  )

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Notes)

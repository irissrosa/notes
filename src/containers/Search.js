import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { filterNotes } from '../actions/actionCreators'
import Search from '../components/Search'

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      onSearch: filterNotes,
    },
    dispatch
  )

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Search)

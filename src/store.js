import { createStore, applyMiddleware, compose } from 'redux'
import { rootReducer } from './reducers'
import rootEpic from './epics'
import { createEpicMiddleware } from 'redux-observable'

const epicMiddleware = createEpicMiddleware()
const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

export default function configureStore() {
  const store = createStore(rootReducer, composeEnhancer(applyMiddleware(epicMiddleware)))
  epicMiddleware.run(rootEpic)
  return store
}

import React, { Fragment } from 'react'
import Notes from './containers/Notes'
import Search from './containers/Search'
import './App.css'

const App = props => (
  <Fragment>
    <Search />
    <Notes />
  </Fragment>
)

export default App

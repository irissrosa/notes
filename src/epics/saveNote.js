import { ofType } from 'redux-observable'
import { empty } from 'rxjs'
import { flatMap } from 'rxjs/operators'
import { EVENTS } from '../actions/actionTypes'
import { getNotes } from '../reducers/notes'

export default (action$, state$) =>
  action$.pipe(
    ofType(EVENTS.SAVE_NOTE, EVENTS.DELETE_NOTE),
    flatMap(() => {
      const state = state$.value

      const validNotes = getNotes(state).reduce((filtered, note) => {
        const { text, title } = note
        if ((title && title !== '') || (text && text !== '')) {
          filtered.push({
            ...note,
            edit: false,
          })
        }
        return filtered
      }, [])

      localStorage.setItem('notes', JSON.stringify(validNotes))
      return empty()
    })
  )

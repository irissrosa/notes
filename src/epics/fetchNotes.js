import { ofType } from 'redux-observable'
import { of } from 'rxjs'
import { flatMap } from 'rxjs/operators'
import { EVENTS } from '../actions/actionTypes'
import { setNotes } from '../actions/actionCreators'
import { getLocalStorageNotes } from '../reducers/notes'

export default (action$, state$) =>
  action$.pipe(
    ofType(EVENTS.FETCH_NOTES),
    flatMap(() => {
      const notes = getLocalStorageNotes()
      return of(setNotes(notes))
    })
  )

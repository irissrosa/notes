import { combineEpics } from 'redux-observable'
import saveNote from './saveNote'
import fetchNotes from './fetchNotes'

export default combineEpics(saveNote, fetchNotes)

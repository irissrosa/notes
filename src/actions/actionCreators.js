import { EVENTS } from './actionTypes'

export const createNote = () => ({
  type: EVENTS.CREATE_NOTE,
})
export const deleteNote = index => ({
  type: EVENTS.DELETE_NOTE,
  payload: index,
})
export const editNote = index => ({
  type: EVENTS.EDIT_NOTE,
  payload: index,
})
export const saveNote = note => ({
  type: EVENTS.SAVE_NOTE,
  payload: note,
})
export const filterNotes = s => ({
  type: EVENTS.FILTER_NOTES,
  payload: s,
})
export const fetchNotes = () => ({
  type: EVENTS.FETCH_NOTES,
})
export const setNotes = notes => ({
  type: EVENTS.SET_NOTES,
  payload: notes,
})

export const EVENTS = {
  CREATE_NOTE: 'notes/CREATE',
  DELETE_NOTE: 'notes/DELETE',
  EDIT_NOTE: 'notes/EDIT',
  SAVE_NOTE: 'notes/SAVE',
  FILTER_NOTES: 'notes/FILTER',
  FETCH_NOTES: 'notes/FETCH',
  SET_NOTES: 'notes/SET',
}

import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Grid from '@material-ui/core/Grid'
import { withStyles } from '@material-ui/core/styles'
import Note from './Note'
import Menu from './Menu'

const styles = theme => ({
  root: {
    padding: 20,
  },
})

class Notes extends Component {
  componentDidMount() {
    this.props.onInit()
  }
  render() {
    const { list, onDeletePress, onEditPress, onSavePress, onAddPress, classes } = this.props
    return (
      <div className={classes.root}>
        <Grid container spacing={16}>
          {list.map((note, index) => (
            <Grid key={index} item xs={3}>
              <Note
                isEditing={note.edit || false}
                title={note.title}
                text={note.text}
                onDeletePress={() => onDeletePress(index)}
                onEditPress={() => onEditPress(index)}
                onSavePress={data => onSavePress({ ...data, index })}
              />
            </Grid>
          ))}
          <Grid item xs={3}>
            <Menu onAddPress={onAddPress} />
          </Grid>
        </Grid>
      </div>
    )
  }
}

Notes.propTypes = {
  list: PropTypes.array.isRequired,
  classes: PropTypes.object.isRequired,
  onDeletePress: PropTypes.func.isRequired,
  onEditPress: PropTypes.func.isRequired,
  onSavePress: PropTypes.func.isRequired,
  onInit: PropTypes.func.isRequired,
}

export default withStyles(styles)(Notes)

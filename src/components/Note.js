import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import Divider from '@material-ui/core/Divider'
import TextField from '@material-ui/core/TextField'
import NoteOptions from './NoteOptions'
import { withStyles } from '@material-ui/core/styles'
import Remarkable from 'remarkable'
import ReactHtmlParser from 'react-html-parser'

const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
  },
  content: {
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    minHeight: 250,
  },
  input: {
    width: '100%',
  },
})

class Note extends Component {
  state = {
    title: this.props.title,
    text: this.props.text,
  }

  resetState = () => {
    this.setState({
      title: this.props.title,
      text: this.props.text,
    })
  }

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    })
  }

  render() {
    const { title, text, onDeletePress, onEditPress, onSavePress, isEditing, classes } = this.props
    const truncatedText = text ? (text.length > 250 ? text.substring(0, 250) + '...' : text) : ''
    const md = new Remarkable()
    const htmlText = ReactHtmlParser(md.render(truncatedText))
    return (
      <Paper className={classes.root}>
        <div className={classes.content}>
          <Typography variant="h5" component="h3">
            {isEditing ? (
              <TextField
                className={classes.input}
                label="Title"
                autoFocus
                margin="normal"
                value={this.state.title}
                onChange={this.handleChange('title')}
              />
            ) : (
              title
            )}
          </Typography>
          <Typography component="p">
            {isEditing ? (
              <TextField
                className={classes.input}
                label="Text"
                multiline
                margin="normal"
                value={this.state.text}
                onChange={this.handleChange('text')}
              />
            ) : (
              htmlText
            )}
          </Typography>
        </div>
        <Divider />
        <NoteOptions
          isEditing={isEditing}
          onEditPress={() => {
            this.resetState()
            onEditPress()
          }}
          onDeletePress={onDeletePress}
          onSavePress={() => onSavePress(this.state)}
        />
      </Paper>
    )
  }
}

Note.defaultProps = {
  isEditing: false,
}
Note.propTypes = {
  classes: PropTypes.object.isRequired,
  isEditing: PropTypes.bool,
  onDeletePress: PropTypes.func.isRequired,
  onEditPress: PropTypes.func.isRequired,
  onSavePress: PropTypes.func.isRequired,
  text: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
}
export default withStyles(styles)(Note)

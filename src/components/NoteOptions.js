import React from 'react'
import PropTypes from 'prop-types'
import BottomNavigation from '@material-ui/core/BottomNavigation'
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction'
import CreateIcon from '@material-ui/icons/Create'
import DeleteIcon from '@material-ui/icons/Delete'
import SaveIcon from '@material-ui/icons/Save'
import CancelIcon from '@material-ui/icons/Cancel'

const NoteOptions = ({ onEditPress, onDeletePress, onSavePress, isEditing }) => (
  <div>
    <BottomNavigation showLabels>
      {isEditing ? (
        [
          <BottomNavigationAction label="Save" icon={<SaveIcon />} onClick={onSavePress} />,
          <BottomNavigationAction label="Cancel" icon={<CancelIcon />} onClick={onEditPress} />,
        ]
      ) : (
        <BottomNavigationAction label="Edit" icon={<CreateIcon />} onClick={onEditPress} />
      )}
      <BottomNavigationAction label="Delete" icon={<DeleteIcon />} onClick={onDeletePress} />
    </BottomNavigation>
  </div>
)

NoteOptions.propTypes = {
  onEditPress: PropTypes.func.isRequired,
  onDeletePress: PropTypes.func.isRequired,
  onSavePress: PropTypes.func.isRequired,
  isEditing: PropTypes.bool,
}

export default NoteOptions

import React from 'react'
import PropTypes from 'prop-types'
import Fab from '@material-ui/core/Fab'
import AddIcon from '@material-ui/icons/Add'
import Paper from '@material-ui/core/Paper'
import { withStyles } from '@material-ui/core/styles'

const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    textAlign: 'center',
    backgroundColor: 'transparent',
    height: 305,
  },
  btn: {
    marginTop: 120,
  },
})

const Menu = ({ onAddPress, classes }) => (
  <Paper elevation={1} className={classes.root}>
    <Fab className={classes.btn} color="primary" aria-label="Add" onClick={onAddPress}>
      <AddIcon />
    </Fab>
  </Paper>
)

Menu.propTypes = {
  classes: PropTypes.object.isRequired,
  onAddPress: PropTypes.func.isRequired,
}
export default withStyles(styles)(Menu)

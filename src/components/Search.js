import React from 'react'
import PropTypes from 'prop-types'
import SearchIcon from '@material-ui/icons/Search'
import Paper from '@material-ui/core/Paper'
import InputBase from '@material-ui/core/InputBase'
import IconButton from '@material-ui/core/IconButton'
import { withStyles } from '@material-ui/core/styles'

const styles = {
  root: {
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: 400,
    marginTop: 20,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  input: {
    marginLeft: 8,
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
}

const Search = ({ onSearch, classes }) => (
  <Paper className={classes.root} elevation={1}>
    <InputBase
      className={classes.input}
      placeholder="Search Notes"
      onChange={e => onSearch(e.target.value)}
    />
    <IconButton className={classes.iconButton} aria-label="Search">
      <SearchIcon />
    </IconButton>
  </Paper>
)

Search.propTypes = {
  onSearch: PropTypes.func.isRequired,
}

export default withStyles(styles)(Search)

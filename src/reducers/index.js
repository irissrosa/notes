import { combineReducers } from 'redux'
import { reducer as notes } from './notes'

export const rootReducer = combineReducers({
  notes,
})

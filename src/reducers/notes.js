import { EVENTS } from '../actions/actionTypes'

const initialState = {
  list: [],
  searchFilter: null,
}

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case EVENTS.SET_NOTES: {
      return {
        ...state,
        list: action.payload,
      }
    }
    case EVENTS.FILTER_NOTES: {
      return {
        ...state,
        searchFilter: action.payload,
      }
    }
    case EVENTS.SAVE_NOTE: {
      const list = state.list.splice(0)
      const { title, text, index } = action.payload
      list[index] = {
        title,
        text,
        edit: false,
      }
      const hasContent = text || title
      if (!hasContent) list.splice(index, 1)
      return {
        ...state,
        list,
      }
    }
    case EVENTS.EDIT_NOTE: {
      const list = state.list.splice(0)
      const note = list[action.payload]
      note.edit = !note.edit
      const hasContent = note.text || note.title
      if (!hasContent) list.splice(action.payload, 1)
      return {
        ...state,
        list,
      }
    }
    case EVENTS.DELETE_NOTE: {
      const list = state.list.splice(0)
      list.splice(action.payload, 1)
      return {
        ...state,
        list,
      }
    }
    case EVENTS.CREATE_NOTE: {
      const list = state.list.concat([
        {
          title: '',
          text: '',
          edit: true,
        },
      ])
      return {
        ...state,
        list,
      }
    }
    default: {
      return state
    }
  }
}

// Selectors
export const getSearchFilter = ({ notes }) => notes.searchFilter

export const getNotes = ({ notes }) => notes.list

export const getLocalStorageNotes = () => {
  const cachedNotes = localStorage.getItem('notes')
  if (cachedNotes) return JSON.parse(cachedNotes)
  return []
}

export const getFilteredNotes = ({ notes }) => {
  const { list } = notes
  let searchTerm = notes.searchFilter

  if (!searchTerm) return list
  searchTerm = searchTerm.toLowerCase()

  const filteredList = list.filter(note => {
    let { text, title } = note

    return (
      (title && title.toLowerCase().indexOf(searchTerm) > -1) ||
      (text && text.toLowerCase().indexOf(searchTerm) > -1)
    )
  })

  return filteredList
}

import { setNotes, saveNote, editNote } from '../actions/actionCreators'
import { reducer } from './notes'

let state

const persistedNotes = [
  {
    title: 'Title 1',
    text: 'Text 1',
    edit: false,
  },
  {
    title: 'Title 2',
    text: 'Text 2',
    edit: false,
  },
  {
    title: 'Title 3',
    text: 'Text 3',
    edit: false,
  },
  {
    title: 'Title 4',
    text: 'Text 4',
    edit: false,
  },
]

beforeEach(() => {
  state = reducer(undefined, {})
})

describe('notes reducer', () => {
  it('should set the notes list', () => {
    const subject = reducer(state, setNotes(persistedNotes))

    expect(subject).toEqual(
      expect.objectContaining({
        list: persistedNotes,
        searchFilter: null,
      })
    )
  })
  it('should save a new note to the list', () => {
    const subject = reducer(
      state,
      saveNote({
        title: 'New',
        text: 'Note',
        index: 0,
      })
    )

    expect(subject).toEqual(
      expect.objectContaining({
        list: [
          {
            title: 'New',
            text: 'Note',
            edit: false,
          },
        ],
        searchFilter: null,
      })
    )
  })
  it('should not save a new note to the list if its content is empty', () => {
    const subject = reducer(
      state,
      saveNote({
        title: '',
        text: '',
        index: 0,
      })
    )

    expect(subject).toEqual(
      expect.objectContaining({
        list: [],
        searchFilter: null,
      })
    )
  })
  it('should remove an empty note from the list', () => {
    const subject = reducer(
      reducer(
        {
          list: [
            {
              title: 'Title 1',
              text: 'Text 1',
              edit: false,
            },
            {
              title: '',
              text: '',
              edit: true,
            },
          ],
          searchFilter: null,
        },
        {}
      ),
      editNote(1)
    )

    expect(subject).toEqual(
      expect.objectContaining({
        list: [
          {
            title: 'Title 1',
            text: 'Text 1',
            edit: false,
          },
        ],
        searchFilter: null,
      })
    )
  })
})
